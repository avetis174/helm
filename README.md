# Базовые функции
- [Основы работы с Helm чартами и темплейтами](https://habr.com/ru/post/547682/)
- [Официальная документация на русском](https://helm.sh/ru/docs/)
- [The Chart Best Practices Guide](https://helm.sh/docs/chart_best_practices/)

# Helm Repository
- [Как использовать GitLab в качестве реестра Helm-чартов](https://dzen.ru/a/YtNE1dzV4H8fJ8cu)
- [Helm chart repository on Sonatype Nexus OSS](https://medium.com/linux-shots/helm-chart-repository-on-sonatype-nexus-oss-fcf6f7c7498e)
