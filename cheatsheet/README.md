# Шпаргалка по использованию Helm

Helm — это менеджер пакетов для Kubernetes, который облегчает установку и управление приложениями. В данной шпаргалке описаны основные команды и концепции, необходимые для работы с Helm.

## Основные компоненты Helm

- **Chart**: Пакет, содержащий все необходимые ресурсы для установки приложения в Kubernetes.
- **Release**: Установленная и управляемая версия Chart'а.
- **Repository**: Хранилище Chart'ов, из которого Helm может загружать пакеты.

---

## Установка Helm

Для установки Helm на локальную машину выполните следующие шаги:

1. Загрузите и установите Helm:
   ```bash
   curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
   ```

2. Проверьте установку:
   ```bash
   helm version
   ```

---

## Основные команды Helm

### Работа с репозиториями

#### Добавить репозиторий

```bash
helm repo add <имя> <URL-репозитория>
```
Пример:
```bash
helm repo add stable https://charts.helm.sh/stable
```

#### Обновить локальный кэш репозиториев

```bash
helm repo update
```

#### Просмотреть список доступных репозиториев

```bash
helm repo list
```

---

### Установка и управление Chart'ами

#### Установить Chart

```bash
helm install <имя-релиза> <репозиторий>/<chart>
```
Пример:
```bash
helm install my-release stable/nginx
```

#### Обновить установку (release)

```bash
helm upgrade <имя-релиза> <репозиторий>/<chart>
```
Пример:
```bash
helm upgrade my-release stable/nginx
```

#### Удалить релиз

```bash
helm uninstall <имя-релиза>
```
Пример:
```bash
helm uninstall my-release
```

#### Просмотреть список установленных релизов

```bash
helm list
```

---

### Просмотр информации

#### Получить информацию о релизе

```bash
helm status <имя-релиза>
```
Пример:
```bash
helm status my-release
```

#### Просмотреть историю релиза

```bash
helm history <имя-релиза>
```
Пример:
```bash
helm history my-release
```

#### Просмотреть все доступные версии Chart'а

```bash
helm search repo <репозиторий>/<chart>
```
Пример:
```bash
helm search repo stable/nginx
```

---

## Работа с Chart'ами

#### Создать новый Chart

```bash
helm create <имя-chart>
```
Пример:
```bash
helm create my-chart
```

#### Упаковать Chart в архив

```bash
helm package <путь-к-chart>
```
Пример:
```bash
helm package my-chart
```

#### Просмотреть содержимое Chart

```bash
helm show all <репозиторий>/<chart>
```
Пример:
```bash
helm show all stable/nginx
```

---

## Параметры и значения

#### Установить Chart с параметрами из файла

```bash
helm install <имя-релиза> <репозиторий>/<chart> -f <путь-к-файлу>
```
Пример:
```bash
helm install my-release stable/nginx -f values.yaml
```

#### Установить Chart с параметрами, заданными в командной строке

```bash
helm install <имя-релиза> <репозиторий>/<chart> --set <ключ>=<значение>
```
Пример:
```bash
helm install my-release stable/nginx --set service.type=NodePort
```

---

## Отладка

#### Проверить Chart на ошибки

```bash
helm lint <путь-к-chart>
```
Пример:
```bash
helm lint my-chart
```

#### Просмотреть генерируемые манифесты Kubernetes без установки

```bash
helm template <имя-релиза> <путь-к-chart>
```
Пример:
```bash
helm template my-release my-chart
```

---

## Полезные ссылки

- Официальная документация Helm: [helm.sh/docs](https://helm.sh/docs/)
- Репозиторий Helm на GitHub: [github.com/helm/helm](https://github.com/helm/helm)

Эта шпаргалка должна помочь вам быстро освоить основные команды и концепции работы с Helm.
